const mongo = require('mongodb').MongoClient;
const faker = require('faker')

const url = 'mongodb://localhost:27017';
const dbName = 'myproject';

const main = async () => {
    try {
        const client = await mongo.connect(url, {useUnifiedTopology:true});
        const db = client.db('simple-project');
        const collection = db.collection('etracker');
     
        await collection.deleteMany({});

        for (let i = 0; i < 10; i++){
            collection.insertOne({
                nama : String,
                used : String, 
                expense : Number,
                category : String
            })
        }
      // client.close();  
    } catch (error) {
        console.log('Error: ',error);
    };
}

main();