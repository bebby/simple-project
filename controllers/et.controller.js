const Etracker = require('../models/et.model.js');
const redis = require('redis');
const { buildSchema } = require('graphql');
const expressGraphql = require('express-graphql');


//schema
const schema = buildSchema(`
type Etracker {
    nama : String,
    used : String, 
    expense : Int,
    category : String
}
type Query{
    etrackers:[Etracker],
    etracker(id:String!): Etracker,
    etrackerByCat(category:String!): [Etracker],
    etrackerUseRedis(id:String!):Etracker
}
type Mutation{
    insertEtracker(nama:String!, used:String!, expense:Int!, category:String!):Etracker,
    updateEtracker(id:String!, nama:String, used:String, expense:Int, category:String):Etracker,
    deleteEtracker(id: String!): Etracker
}`)

//resolve
const root ={
    etrackers: async() => {
        return await Etracker.find();
    },
    etracker:async({id}) => {
        return await Etracker.findOne({_id : id})
    },
    // etrackerUseRedis:async({id}) => {
    //     result = await Etracker.findAllRedis({_id : id})
    //     return result;
    // },
    etrackerByCat:async({category}) => {
        return await Etracker.find({category:category})
    },
    insertEtracker: async({nama, used, category, expense}) => {
        const newEtracker = new Etracker({
            nama:nama,
            used:used,
            expense:expense,
            category:category
        })
        return await newEtracker.save();
    }, 
    updateEtracker: async({id, nama, used, category, expense}) => {
        const updateEt ={};
        if (nama) updateEt.nama = nama;
        if (used) updateEt.used = used;
        if (expense) updateEt.expense = expense;
        if (category) updateEt.category = category;
        return await Etracker.findOneAndUpdate({_id : id},{$set:updateEt},{useFindAndModify:false});
    }, 
    deleteEtracker: async({id}) => {
        const etracker = await Etracker.findOne({ _id:id})
        await Etracker.deleteOne({_id:id})
        return etracker;
    }
}

const graphql = expressGraphql({
    schema: schema,
    rootValue: root,
    graphiql:true
})

module.exports ={
    graphql
}