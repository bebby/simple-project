const express = require('express');
const mongoose = require('mongoose');
const mongo = require('mongodb').MongoClient;
const faker = require('faker')

const url = 'mongodb://localhost:27017';
const dbName = 'myproject';
const app = express();



const connect = async (db) => {
    try{
        await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology:true, dbName });
        console.log("Successfully connected to the database.");
    }catch(err){
        console.log("Could not connect to the database. Error ",err);
        process.exit();
    }
}
connect();

async function main(){
    let client = await mongo.connect(url,{useUnifiedTopology: true});
    const db = client.db('simple-project');
    const collection = db.collection('etracker');
    // await collection.deleteMany({});
    const chats = await collection.find({});
    client.close();
}
main();

app.get('/',(req, res) => {
    res.json({message : "WELCOME"})    
})

require('./routes/et.routes.js')(app);

app.listen(3020,() => {
    console.log('server is running ....');
})