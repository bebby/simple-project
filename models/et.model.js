const mongoose = require('mongoose');

const EtrackerSchema = mongoose.Schema({
    nama : String,
    used : String, 
    expense : Number,
    category : String
},
{
    timestamps:true
}
);

module.exports = mongoose.model('Etracker', EtrackerSchema);