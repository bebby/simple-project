const etrackers = require('../controllers/et.controller.js')

module.exports = (app) => {
    app.use('/graphql', etrackers.graphql)
};